package pt.ulusofona.lp2.livroDeReceitas;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.*;

// classe puramente imperativa
public class AppEstatisticas {

    private static List<Receita> receitas;

    private AppEstatisticas() {}

    private static void inicializaListaReceitas() {
        receitas = new ArrayList<>();

        receitas.add(new Receita("Amêijoas à Bulhão Pato", "Portuguesa", 222, false));
        receitas.add(new Receita("Pataniscas de Sardinha", "Portuguesa", 100, false));
        receitas.add(new Receita("Arroz de Pato", "Portuguesa", 300, false));
        receitas.add(new Receita("Arroz-Doce", "Portuguesa", 500, true));
        receitas.add(new Receita("Salada de Ananás e Limão", "Portuguesa", 50, true));
        receitas.add(new Receita("Panna Cota", "Italiana", 370, true));
        receitas.add(new Receita("Risotto Primavera", "Italiana", 320, false));
        receitas.add(new Receita("Pizza de Salmão e Rúcola", "Italiana", 250, false));
        receitas.add(new Receita("Tagliatelle com Amêijoas e Coentros", "Italiana", 180, false));
        receitas.add(new Receita("Salada Caprese", "Italiana", 76, true));
        receitas.add(new Receita("Chilli com Carne", "Mexicana", 340, false));
        receitas.add(new Receita("Caril de Tofu", "Indiana", 190, true));
        receitas.add(new Receita("Chicken Tikka Masala", "Indiana", 247, false));
        receitas.add(new Receita("Frango com Ginguba", "Angolana", 310, false));
        receitas.add(new Receita("Caril de Legumes", "Angolana", 270, true));
        receitas.add(new Receita("Costeletas de Vitela com chimichurri", "Argentina", 390, false));
    }

    public static void main(String[] args) {

        inicializaListaReceitas();

        // exercício 0 - mostra o número de receitas
        long numReceitas = receitas.stream()
                .collect(counting());
        System.out.println("número de receitas = " + numReceitas);

        // exercício 1 - mostra o número de receitas com mais do que 60 calorias

        // exercício 2 - mostra o nome das receitas italianas e vegetarianas com mais do que 60 calorias

        // exercício 3 - mostra o nome das 5 receitas mais calóricas, ordenadas da mais calórica
        // para a menos calórica, que não sejam vegetarianas

        // exercício 4 - mostra quais os tipos (Portuguesa, Italiana, etc.) para os quais existem receitas
        // vegetarianas

    }
}
