package pt.ulusofona.lp2.livroDeReceitas;

public class Receita {
    private String nome;
    private String tipo;
    private int numCalorias;
    private boolean vegetariana;

    public Receita(String nome, String tipo, int numCalorias, boolean vegetariana) {
        this.nome = nome;
        this.tipo = tipo;
        this.numCalorias = numCalorias;
        this.vegetariana = vegetariana;
    }

    public String getNome() {
        return nome;
    }

    public String getTipo() {
        return tipo;
    }

    public int getNumCalorias() {
        return numCalorias;
    }

    public boolean isVegetariana() {
        return vegetariana;
    }
}
