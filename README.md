## Exercício de programação com streams

Este exercício faz parte da disciplina de Linguagens de Programação II da Universidade Lusófona.

## Como começar?

No Intellij, seleccionar File > New > Project From Version Control > Git

![](/src/main/resources/intellij-screen.png)

No Git Repository URL colocar o endereço deste repositório (ex: https://bitbucket.org/ULHT-LP2-2017-2018/livro-de-receitas)

O Intellij fará um clone do projecto para o vosso disco (para a pasta que indicarem) 
e configurará automaticamente o projecto de forma a que o possam compilar e correr.

## E a seguir?

Na classe AppEstatísticas, na função main, existem 4 comentários que indicam 4 exercícios para implementar. 
O exercício 0 já está implementado para servir de exemplo.

## Como entregar?

Por se tratar de um exercício das aulas teóricas, terá que ser entregue em papel no início da aula teórica.
O professor irá pedir a alguns alunos, aleatoriamente, para explicarem a resolução que apresentaram do exercício.
